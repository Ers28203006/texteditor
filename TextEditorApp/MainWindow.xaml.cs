﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Documents;

namespace TextEditorApp
{
    public partial class MainWindow : Window
    {
        Timer Timer { get; set; }
        public MainWindow()
        {
            InitializeComponent();

            Timer = new Timer(TimerCallback, null, 0, 10000);
        }
        private void TimerCallback(object o)
        {
            SaveFile();
        }
        private void ExitItemClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SaveAsItemClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|RichText Files (*.rtf)|*.rtf|XAML Files (*.xaml)|*.xaml|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                TextRange doc = new TextRange(docBox.Document.ContentStart, docBox.Document.ContentEnd);
                using (FileStream fileStream = File.Create(saveFileDialog.FileName))
                {
                    if (Path.GetExtension(saveFileDialog.FileName).ToLower() == ".rtf")
                        doc.Save(fileStream, DataFormats.Rtf);
                    else if (Path.GetExtension(saveFileDialog.FileName).ToLower() == ".txt")
                        doc.Save(fileStream, DataFormats.Text);
                    else
                        doc.Save(fileStream, DataFormats.Xaml);
                }
                MessageBox.Show(saveFileDialog.FileName);
            }
        }


        private void SaveItemClick(object sender, RoutedEventArgs e)
        {
            SaveFile();
        }

        private void SaveFile( )
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = "doc.txt";
            TextRange doc = new TextRange(docBox.Document.ContentStart, docBox.Document.ContentEnd);
            using (FileStream fileStream = File.Create(saveFileDialog.FileName))
            {
                if (Path.GetExtension(saveFileDialog.FileName).ToLower() == ".txt")
                    doc.Save(fileStream, DataFormats.Text);
            }
        }

        private void OpenAsItemClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "XAML Files(*.xaml)|*.xaml|RichText Files (*.rtf)|*.rtf|All Files (*.*)|*.*";

            if (openFile.ShowDialog() == true)
            {
                TextRange textRangeOpen = new TextRange(docBox.Document.ContentStart, docBox.Document.ContentEnd);
                using (FileStream stream = File.Open(openFile.FileName, FileMode.Open))
                {
                    if (Path.GetExtension(openFile.FileName).ToLower() == "*.rtf")
                        textRangeOpen.Load(stream, DataFormats.Rtf);
                    else if (Path.GetExtension(openFile.FileName).ToLower() == ".txt")
                        textRangeOpen.Load(stream, DataFormats.Text);
                    else
                        textRangeOpen.Save(stream, DataFormats.Xaml);
                }
            }
        }

        private void CreateItemClick(object sender, RoutedEventArgs e)
        {
            docBox.Document = new FlowDocument();
        }
    }
}
